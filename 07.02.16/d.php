<?php
echo 'this is a simple string';
echo 'you can also have embedded newlines in 
strings this way as it is 
okay to do';
// outputs: Arnold once said : "I'will be back"
//echo 'Arnold once said:  "I\ ' ll be back" ' ;
echo 'Arnold once said: "I\'ll be back" ';
// outputs:you deleted c:\*.*?
    echo 'you deleted c:\\ *.*?';
    //outputs:you deleted c:\*.*?
    echo 'you deleted c:\*.*?';
    //outputs :variables do not $expand $either
        echo 'variables do not $expand $either';
?>